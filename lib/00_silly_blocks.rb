def reverser
  yield.split(' ').map(&:reverse).join(' ') if block_given?
end

def adder(n = 1)
  return yield + n if block_given?
end

def repeater(n = 1)
  n.times { yield } if block_given?
end
