require 'time'

def measure(n = 1)
  before_block = Time.now
  n.times { yield } if block_given?
  (Time.now - before_block) / n
end
